import React, { useEffect, useRef } from "react";
import { FormattedMessage } from "react-intl";
import { DraggablePanel } from "./DraggablePanel";
import { MediaPipeDevice } from "../../systems/userinput/devices/mediapipe-controller";

export function MediaPipeDebugPanel() {
  const canvasContainer = useRef(null);
  const userinput = AFRAME.scenes[0].systems.userinput;
  useEffect(() => {
    for (const adi of userinput.activeDevices.items) {
      if (adi instanceof MediaPipeDevice) {
        canvasContainer.current.innerHTML = "";
        canvasContainer.current.append(adi.canvasElement);
      }
    }
  }, [canvasContainer, userinput]);
  return (
      <DraggablePanel
        title={<FormattedMessage id="mediapipe-debug-panel.mediapipe-debug-title" defaultMessage="MediaPipe Debug" />}
        isRoot
        border
        collapsed={false}
      >
        <div>
          <div ref={ canvasContainer } style={{ display: "flex", height: "100%", width: "100%", justifyContent: "center", alignItems: "center" }}>
            <em style={{color: "white", marginBottom: "0.5rem"}}>Loading...</em>
          </div>
        </div>
      </DraggablePanel>
  );
}

MediaPipeDebugPanel.propTypes = {
    // TODO
};
